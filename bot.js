// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityHandler, MessageFactory } = require('botbuilder');
const { LuisRecognizer } = require('botbuilder-ai')

class EchoBot extends ActivityHandler {
    constructor() {
        super();

        const LUISconnector = new LuisRecognizer({
            applicationId: process.env.LuisAppId,
            endpointKey: process.env.LuisApiKey,
            endpoint: `https://${process.env.LuisAppHostName}.api.cognitive.microsoft.com`
        }, {
            includeAllIntents: true
        }, true)

        // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        this.onMessage(async (context, next) => {
            context.activity.text = context.activity.text.replace('slash', '/');
            const result = await LUISconnector.recognize(context);
            const obtainedEntities = result.luisResult.entities;
            const resultArray = []

            const medicineEntities = obtainedEntities.filter((item) => item.type === 'Medicine')

            // Creating individual object for each type of medicine and pushing it to resultArray
            medicineEntities.forEach((item) => {
                const newObj = {};
                if(!item.children.empty) {
                    const childEntities = item.children
    
                    childEntities.forEach((item) => { 
                        if(item.type === 'StrengthOfMedicine' && newObj['StrengthOfMedicine'] !== undefined){
                            const addedStrength = item.entity
                            newObj['StrengthOfMedicine'] += `, ${addedStrength}`
                        } else {
                            return newObj[item.type] = item.entity
                        }
                    });
                }   
                resultArray.push(newObj)
            })

            // Fetching brands and form type from other entites, to be used if not present inside Medicine entity
            const brandsArray = obtainedEntities.filter((item) => item['type'] === 'brand' );
            const FormArray = obtainedEntities.filter((item) => item['type'] === 'Form Type' );

            // If the values are not present in medicine entity, update it from the above array
            for(let i = 0; i < resultArray.length; ++i) {
                if(!resultArray[i]['brand']) {
                    resultArray[i]['brand'] = brandsArray[i]['entity']
                }
                if(!resultArray[i]['Form Type']) {
                    resultArray[i]['Form Type'] = FormArray[i]['entity']
                }
            }
            
            // Returning resultArray
            await context.sendActivity(JSON.stringify(resultArray));
            await next();
        });
    }
}

module.exports.EchoBot = EchoBot;
